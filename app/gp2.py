import os
from time import monotonic
from subprocess import CalledProcessError, CompletedProcess, PIPE, Popen, STDOUT, TimeoutExpired
from hashlib import sha1
from uuid import uuid4


class Interpreter(object):

    def __init__(self, gp2c, gp2i, compiler_timeout = None, program_timeout = None):
        self._gp2c = gp2c
        self._gp2i = gp2i
        self._cache = {}
        self._compiler_timeout = compiler_timeout
        self._program_timeout = program_timeout

    def interpret(self, program, graph):
        program_hash =  sha1(program.encode('utf-8')).hexdigest()
        source, target = self._cache.get(program_hash, (None, None))

        if source is None or target is None:
            source = '/tmp/' + str(uuid4())
            os.mkdir(source)

            with open(source + '/program', 'w') as f:
                f.write(program)

            target = '/tmp/' + str(uuid4())

            error = self._compile(source, target)
            if error:
                return None, error, 0

            self._cache[program_hash] = (source, target)

        with open(source + '/graph', 'w') as f:
            f.write(graph)

        start = monotonic()
        success, output = self._execute(source, target)

        return output if success else None, None if success else output, int((monotonic() - start) * 1000000)

    def _compile(self, source, target):
        try:
            self._run([self._gp2c, source, target, 'program'], timeout=self._compiler_timeout)
        except TimeoutExpired as e:
            return b'The maximum compiler execution time was exceeded!'
        except CalledProcessError as e:
            return self._sanitize(e.output or b'Failed to compile program!', source + '/program', target)

    def _execute(self, source, target):
        try:
            return True, self._run([self._gp2i, source, target, 'graph'], timeout=self._program_timeout).stdout
        except TimeoutExpired as e:
            return False, b'The maximum program execution time was exceeded!'
        except CalledProcessError as e:
            return False, self._sanitize(e.output or b'Failed to execute program!', source + '/program', source + '/graph')

    def _run(self, *popenargs, input=None, capture_output=False, timeout=None, **kwargs):
        if input is not None:
            if kwargs.get('stdin') is not None:
                raise ValueError('stdin and input arguments may not both be used.')
            kwargs['stdin'] = PIPE

        if capture_output:
            if kwargs.get('stdout') is not None or kwargs.get('stderr') is not None:
                raise ValueError('stdout and stderr arguments may not be used with capture_output.')
            kwargs['stdout'] = PIPE
            kwargs['stderr'] = PIPE

        with Popen(*popenargs, **kwargs, env=os.environ.copy(), preexec_fn=os.setsid, stdout=PIPE, stderr=STDOUT) as process:
            try:
                stdout, stderr = process.communicate(input, timeout=timeout)
            except TimeoutExpired as e:
                os.killpg(os.getpgid(process.pid), 15)
                process.wait()
                raise
            except:
                os.killpg(os.getpgid(process.pid), 15)
                raise
            retcode = process.poll()
            if retcode:
                raise CalledProcessError(retcode, process.args, output=stdout, stderr=stderr)
        return CompletedProcess(process.args, retcode, stdout, stderr)

    def _sanitize(self, data, path_a, path_b):
        data = data.split(b'Generating program code...')[0];

        if b'No targets specified and no makefile found.' in data:
            data = b'Failed to compile program!'
        elif b' Segmentation fault ' in data:
            data = b'A segmentation fault occured!'

        return data.replace(bytes(path_a, 'utf-8'), b'').replace(bytes(path_b, 'utf-8'), b'').replace(b'  ', b' ')
