import os
from time import sleep
from sys import stdout
from gp2 import Interpreter
from grpc import server, Server
from concurrent.futures import ThreadPoolExecutor

from gp2i_pb2 import *
from gp2i_pb2_grpc import *


class Service(GP2IServicer):

    def __init__(self, interpreter):
        self._interpreter = interpreter

    def Interpret(self, request, context):
        if request.program is None or request.program == '':
            return Response(error='A program must be provided.', time=0)

        if request.graph is None or request.graph == '':
            return Response(error='An input graph must be provided.', time=0)

        graph, error, time = self._interpret(request.program, request.graph)

        if graph is None or graph == '':
            return Response(error=error, time=time)

        return Response(graph=graph, time=time)

    def _interpret(self, program, graph):
        try:
            return self._interpreter.interpret(program, graph)
        except BaseException as e:
            print(e)
            stdout.flush()
            return None, 'An unexpected error occured.', 0


class Factory(object):

    @staticmethod
    def make(service = None):
        MB = 1024 * 1024
        GRPC_CHANNEL_OPTIONS = [('grpc.max_message_length', 256 * MB), ('grpc.max_receive_message_length', 256 * MB)]

        s = server(ThreadPoolExecutor(max_workers=16), options=GRPC_CHANNEL_OPTIONS)
        add_GP2IServicer_to_server(service or Factory._make_service(), s)
        s.add_insecure_port('[::]:9111')

        return s

    @staticmethod
    def _make_service(gp2c_path = None, gp2i_path = None):
        return Service(
            Interpreter(
                gp2c_path or 'gp2c.sh',
                gp2i_path or 'gp2i.sh',
                int(os.environ.get('GP2_COMPILER_TIMEOUT', '0')) or None,
                int(os.environ.get('GP2_PROGRAM_TIMEOUT', '0')) or None
            )
        )


def main():
    s = Factory.make()
    s.start()

    try:
        while True:
            sleep(60 * 60 * 24)
    except KeyboardInterrupt:
        s.stop(0)


if __name__ == '__main__':
    main()
